from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='myn_ad_server_utils',
    version='1.0.0',
    description='myn_ad_server_utils service',
    url='git@bitbucket.org/myntelligence_v2/myn_ad_server_utils.git',
    author='myntelligence',
    packages=find_packages(exclude=['tests*']),
    python_requires='>=3.6',
    install_requires=requirements,
)
