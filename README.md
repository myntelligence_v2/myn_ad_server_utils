# Ad Server Utils Service
Service providing mapping for platforms/channels from ad servers settings (adform, campaign manager, reports service)

# Installation
    pip install git+https://bitbucket.org/myntelligence_v2/myn_ad_server_utils.git
  
# Usage
    from from myn_ad_server_utils.utils import get_platforms_models_by_channel