import os
from typing import Optional
import redis
import json

from myn_ad_server_utils.snapshot.snapshot_example import SNAPSHOT_EXAMPLE


class SnapshotService:
    PLATFORM_KEY = 'platforms'
    def __init__(self, client = None):
        self._client: redis.Redis = client or self._default_client()
        self._snapshot: Optional[dict] = None

    @staticmethod
    def _default_client():
        return redis.Redis(
            host=os.environ.get('REDIS_HOST', 'localhost'),
            port=os.environ.get('REDIS_PORT', 6379),
            db=6,
        )

    def _get_snapshot(self) -> dict:
        raw_snapshot: str = self._client.get(self.PLATFORM_KEY)
        return json.loads(raw_snapshot)

    def get(self, key):
        if not self._snapshot:
            self._snapshot = self._get_snapshot()
        return self._snapshot[key]


class FakeSnapshotService(SnapshotService):
    def _get_snapshot(self) -> dict:
        return SNAPSHOT_EXAMPLE
