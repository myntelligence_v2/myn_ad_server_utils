from contextvars import ContextVar, Token

from myn_ad_server_utils.snapshot.snapshot import SnapshotService


class SnapshotContextVar:
    """
    There are no way to inherit from `ContextVar` class
    (only overriding __new__ method).
    """
    def __init__(self, name: str):
        self._snapshot_context: ContextVar[SnapshotService] = ContextVar(name)

    def set(self, value: SnapshotService) -> Token:
        return self._snapshot_context.set(value)

    def get(self, key: str):
        snapshot_service = self._snapshot_context.get()
        return snapshot_service.get(key)

    def reset(self, token: Token) -> None:
        self._snapshot_context.reset(token)

    def __repr__(self) -> str:
        return self._snapshot_context.__repr__()


snapshot = SnapshotContextVar('snapshot')


class SnapshotContextMiddleware:
    """
    Django context middleware that set up snapshot context variable for each request
    """
    def __init__(self, get_response=None) -> None:
        self.get_response = get_response
        super().__init__()

    def __call__(self, request):
        service = SnapshotService()
        token = snapshot.set(service)

        response = self.get_response(request)

        snapshot.reset(token)
        return response
