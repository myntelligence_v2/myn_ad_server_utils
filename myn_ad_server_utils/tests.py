from unittest import TestCase
from .utils import mapping_to_choices, merge_dicts


class TestMappingToChoices(TestCase):
    def test_simple_mapping(self):
        test_data = {"ONE": 1, "TWO": 2, "THREE": 3}
        self.assertCountEqual(
            mapping_to_choices(test_data), [('ONE', 1), ('TWO', 2), ('THREE', 3)]
        )

    def test_nested_mapping(self):
        test_data = {"ONE": {"TWO": 2, "THREE": 3}, "FOUR": {"FIVE": 5}}
        self.assertCountEqual(
            mapping_to_choices(test_data),
            [('ONE', [('TWO', 2), ('THREE', 3)]), ('FOUR', [('FIVE', 5)])],
        )

    def test_hybrid_mapping(self):
        test_data = {"ONE": 1, "TWO": 2, "THREE": {"FOUR": 4, "FIVE": 5}}
        self.assertCountEqual(
            mapping_to_choices(test_data),
            [('ONE', 1), ('TWO', 2), ('THREE', [('FOUR', 4), ('FIVE', 5)])],
        )

    def test_invalid_input(self):
        test_data = 'INVALID'
        self.assertEqual(mapping_to_choices(test_data), [])


class TestMergeDicts(TestCase):

    def test_simple_dict(self):
        test_dict_one = {
            "ONE": [1, 2, 3],
            "TWO": [4, 5, 6],
        }
        test_dict_two = {
            "ONE": [1, 2, 3],
            "THREE": [4, 5, 6],
        }
        self.assertDictEqual(
            merge_dicts([test_dict_one, test_dict_two]),
            {
                "ONE": [1, 2, 3, 1, 2, 3],
                "TWO": [4, 5, 6],
                "THREE": [4, 5, 6],
            }
        )

    def test_different_values_dict(self):
        test_dict_one = {
            "ONE": "1",
            "TWO": 4,
        }
        test_dict_two = {
            "ONE": [1, 2, 3],
            "THREE": 4,
            "FOUR": 5,
        }
        self.assertDictEqual(
            merge_dicts([test_dict_one, test_dict_two]),
            {
                "ONE": ["1", 1, 2, 3],
                "TWO": [4],
                "THREE": [4],
                "FOUR": [5],
            }
        )
