from myn_ad_server_utils.constants.common_integrations import (
    ADFORM, SPOTIFY,
    CAPTIFY, SIZMEK, EDISCOM,
    KETCHUP, ACCUEN, PUBLY,
    CHILI, TALKS_MEDIA,
    ALIBABA,
    COMMON_INTEGRATIONS,
    PAYCLICK,
    ADVICE_ME,
    SOGOU,
    RAKUTEN_ADV,
    BUZZOOLE,
    IGOAL,
    S4M,
    HOOPYGANG,
    OUTBRAIN,
    TENCENT_MUSIC_SPOTIFY,
    SPOTX,
    DEEZER,
    AWIN,
    BLUE,
    ADVERTISME,
    AVRAGE_MEDIA,
    LINKAPPEAL,
    VERTIGO,
    WAZE,
    TOP_PARTNERS,
    MEDIATIQUE,
    SPIN_UP,
    ADPLAY,
    INVIBES,
    DOVE_CONVIENE,
    OVERVIEW_TD,
)
from myn_ad_server_utils.constants.channels import (
    SEARCH, SOCIAL, VIDEO, DISPLAY,
    NATIVE, MOBILE, DEM,
    PROGRAMMATIC_RADIO, DOOH, ADDRESSABLE_TV,
    CHATBOT, OFFLINE_TV, BRANDED_CONTENT, INFLUENCER_MARKETING,
)
from myn_ad_server_utils.constants.integrated_platforms import (
    FACEBOOK,
    INTEGRATED_PLATFORMS,
)

AD_YOU_LIKE = 'ad_you_like'
GOOGLE_YOUTUBE_CTV = 'google_youtube_ctv'
SMARTCLIP = 'smartclip'
XANDR = 'xandr'
REFINE = 'refine'
TRADE_DOUBLER = 'trade_doubler'
AMAZON = 'amazon'
BAIDU = 'baidu'
DISCOVERY_ADS = 'discovery_ads'
HYBRID_AI = 'hybrid_ai'
MEDIA_MATH = 'media_math'
QUANTCAST = 'quantcast'
TENCENT = 'tencent'
THE_TRADE_DESK = 'the_trade_desk'
VERIZON_MEDIA = 'verizon_media'
WIDESPACE = 'widespace'
ZETA_EIKON = 'zeta_eikon'
AD_QUICK_COM = 'ad_quick_com'
TAGGIFY = 'taggify'
VISTAR_MEDIA = 'vistar_media'
HIC_MOBILE = 'hic_mobile'
INSTAL = 'instal'
OGURY = 'ogury'
TABOOLA = 'taboola'
ZEMANTA = 'zemanta'
ADS_WIZZ = 'ads_wizz'
BING = 'bing'
GOOGLE_SA_360 = 'google_sa_360'
YAHOO = 'yahoo'
YANDEX = 'yandex'
LINKEDIN = 'linkedin'
PINTEREST = 'pinterest'
REDDIT = 'reddit'
TIK_TOK = 'tik_tok'
TUMBLR = 'tumblr'
TWITTER = 'twitter'
VK_COM = 'vk_com'
XING = 'xing'
YOUTUBE = 'youtube'
ADOBE_DSP_TUBEMOGUL = 'adobe_dsp_tubemogul'
TEADS = 'teads'
CRITEO = 'criteo'
INSTAGRAM = 'instagram'
GOOGLE_DISPLAY_NETWORK_EXCLUDED = 'google_display_network'
DB_SHAPER = 'db_shaper'
BMC = 'bmc'
ANTEVENIO = 'antevenio'
ARKEERO = 'arkeero'
CLICKPOINT = 'clickpoint'
GEKO = 'geko'
PERFORMAGENCY = 'performagency'
PUREBROS = 'purebros'
UNIVERSAL_APP_CAMPAIGN = 'universal_app_campaign'
MAIN_AD = 'main_ad'
DIGITAL_VENTURE = 'digital_venture'
TWITCH = 'twitch'
SNAP_CHAT = 'snap_chat'
TABRIS = 'tabris'
RANKIT = 'rankit'
MEDIA_MATIC = 'media_matic'
CHANNEL_FACTORY = 'channel_factory'
HEJ = 'hej'
XAXIS = 'xaxis'
SAMSUNG_ADS = 'samsung_ads'
PSAFE = 'psafe'
ZETA_DSP = 'zeta_dsp'
RAI = 'rai'
LA7 = 'la_seven'
PUBLITALIA = 'publitalia'
KWAI = 'kwai'
DV360_EXCLUDED = 'dv360'
DV360_VIRTUAL = 'dv360_virtual'
GOOGLE_DISPLAY_NETWORK_VIRTUAL = 'google_display_network_virtual'
APPLE_SEARCH_ADS = 'apple_search_ads'
MYVIDEO_TARGETING = 'myvideo_targeting'
ONETAG = 'onetag'
INCUBETA = "incubeta"


NOT_INTEGRATED_PLATFORMS = {
    AD_YOU_LIKE: 'Adyoulike',
    FACEBOOK: INTEGRATED_PLATFORMS[FACEBOOK],
    GOOGLE_YOUTUBE_CTV: 'Google (Youtube CTV)',
    SMARTCLIP: 'smartclip DSP',
    XANDR: 'Xandr DSP',
    REFINE: 'Refine Direct',
    TRADE_DOUBLER: 'Tradedoubler DSP',
    ADFORM: COMMON_INTEGRATIONS[ADFORM],
    AMAZON: 'Amazon DSP',
    BAIDU: 'Baidu',
    CAPTIFY: COMMON_INTEGRATIONS[CAPTIFY],
    DISCOVERY_ADS: 'Discovery Ads',
    HYBRID_AI: 'Hybrid.ai DSP',
    MEDIA_MATH: 'MediaMath',
    QUANTCAST: 'Quantcast',
    SPOTIFY: COMMON_INTEGRATIONS[SPOTIFY],
    TENCENT: 'Tencent',
    THE_TRADE_DESK: 'The Trade Desk',
    VERIZON_MEDIA: 'Verizon',
    WIDESPACE: 'Widespace',
    ZETA_EIKON: 'ZETA DSP',
    AD_QUICK_COM: 'AdQuick DSP',
    DV360_EXCLUDED: "DV360 Virtual",
    TAGGIFY: 'Taggify',
    VISTAR_MEDIA: 'Vistar Media DSP',
    HIC_MOBILE: 'Hic Mobile',
    OGURY: 'Ogury',
    OUTBRAIN: COMMON_INTEGRATIONS[OUTBRAIN],
    TABOOLA: 'Taboola',
    ZEMANTA: 'Zemanta',
    ADS_WIZZ: 'AdsWizz',
    TENCENT_MUSIC_SPOTIFY: COMMON_INTEGRATIONS[TENCENT_MUSIC_SPOTIFY],
    BING: 'Bing',
    GOOGLE_SA_360: 'Google SearchAds 360',
    YAHOO: 'Yahoo DSP',
    YANDEX: 'Yandex DSP',
    CHILI: COMMON_INTEGRATIONS[CHILI],
    LINKEDIN: 'Linkedin',
    PINTEREST: 'Pinterest',
    REDDIT: 'Reddit',
    TIK_TOK: 'Tik Tok',
    TUMBLR: 'Tumblr',
    TWITTER: 'Twitter',
    VK_COM: 'VK',
    XING: 'XING Ads',
    YOUTUBE: 'Youtube',
    ADOBE_DSP_TUBEMOGUL: 'Adobe DSP',
    SPOTX: COMMON_INTEGRATIONS[SPOTX],
    TEADS: 'Teads',
    CRITEO: 'Criteo',
    SIZMEK: COMMON_INTEGRATIONS[SIZMEK],
    INSTAL: 'Instal',
    DEEZER: COMMON_INTEGRATIONS[DEEZER],
    INSTAGRAM: 'Instagram',
    GOOGLE_DISPLAY_NETWORK_EXCLUDED: 'Google Display Network Virtual',
    GOOGLE_DISPLAY_NETWORK_VIRTUAL: 'Google Display Network Virtual',
    DB_SHAPER: 'Db Shaper',
    AWIN: COMMON_INTEGRATIONS[AWIN],
    BLUE: COMMON_INTEGRATIONS[BLUE],
    BMC: 'BMC',
    ADVERTISME: COMMON_INTEGRATIONS[ADVERTISME],
    ANTEVENIO: 'Antevenio',
    ARKEERO: 'Arkeero DSP',
    CLICKPOINT: 'CLICKPOINT',
    EDISCOM: COMMON_INTEGRATIONS[EDISCOM],
    GEKO: 'GEKO ADV',
    KETCHUP: COMMON_INTEGRATIONS[KETCHUP],
    PAYCLICK: COMMON_INTEGRATIONS[PAYCLICK],
    PERFORMAGENCY: 'PERFORMAGENCY',
    PUREBROS: 'Pure Bros',
    MAIN_AD: 'MainAd',
    UNIVERSAL_APP_CAMPAIGN: 'Universal App Campaign',
    DIGITAL_VENTURE: 'Digital Venture',
    AVRAGE_MEDIA: COMMON_INTEGRATIONS[AVRAGE_MEDIA],
    LINKAPPEAL: COMMON_INTEGRATIONS[LINKAPPEAL],
    PUBLY: COMMON_INTEGRATIONS[PUBLY],
    TALKS_MEDIA: COMMON_INTEGRATIONS[TALKS_MEDIA],
    VERTIGO: COMMON_INTEGRATIONS[VERTIGO],
    TWITCH: 'Twitch',
    SNAP_CHAT: 'Snapchat',
    WAZE: COMMON_INTEGRATIONS[WAZE],
    TABRIS: 'TABRIS AI',
    ADVICE_ME: COMMON_INTEGRATIONS[ADVICE_ME],
    TOP_PARTNERS: COMMON_INTEGRATIONS[TOP_PARTNERS],
    RANKIT: 'Rankit',
    MEDIA_MATIC: 'MediaMatic',
    CHANNEL_FACTORY: 'Channel Factory',
    HEJ: 'HEJ!',
    MEDIATIQUE: COMMON_INTEGRATIONS[MEDIATIQUE],
    SPIN_UP: COMMON_INTEGRATIONS[SPIN_UP],
    XAXIS: 'XAXIS',
    ACCUEN: COMMON_INTEGRATIONS[ACCUEN],
    ADPLAY: COMMON_INTEGRATIONS[ADPLAY],
    SAMSUNG_ADS: 'Samsung Ads',
    PSAFE: 'Psafe',
    ALIBABA: COMMON_INTEGRATIONS[ALIBABA],
    ZETA_DSP: 'Zeta DSP',
    RAI: 'Rai',
    LA7: 'La7',
    PUBLITALIA: 'Publitalia',
    SOGOU: COMMON_INTEGRATIONS[SOGOU],
    RAKUTEN_ADV: COMMON_INTEGRATIONS[RAKUTEN_ADV],
    BUZZOOLE: COMMON_INTEGRATIONS[BUZZOOLE],
    IGOAL: COMMON_INTEGRATIONS[IGOAL],
    S4M: COMMON_INTEGRATIONS[S4M],
    HOOPYGANG: COMMON_INTEGRATIONS[HOOPYGANG],
    KWAI: 'Kwai',
    INVIBES: COMMON_INTEGRATIONS[INVIBES],
    DV360_VIRTUAL: "DV360 Virtual",
    APPLE_SEARCH_ADS: "Apple Search Ads",
    DOVE_CONVIENE: COMMON_INTEGRATIONS[DOVE_CONVIENE],
    OVERVIEW_TD: COMMON_INTEGRATIONS[OVERVIEW_TD],
    MYVIDEO_TARGETING: "MyVideo Targeting",
    ONETAG: "Onetag",
    INCUBETA: "Incubeta",
}

CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_MAP = {
    SEARCH: (
        BAIDU,
        BING,
        GOOGLE_SA_360,
        YAHOO,
        YANDEX,
        YOUTUBE,
        CAPTIFY,
        SOGOU,
        CRITEO,
        MEDIA_MATIC,
        XAXIS,
        APPLE_SEARCH_ADS,
    ),
    SOCIAL: (
        CHILI,
        LINKEDIN,
        PINTEREST,
        REDDIT,
        TIK_TOK,
        TUMBLR,
        TWITTER,
        VK_COM,
        XING,
        YOUTUBE,
        INSTAGRAM,
        TWITCH,
        SNAP_CHAT,
        TABRIS,
        RANKIT,
        OUTBRAIN,
        ALIBABA,
        TENCENT,
        CAPTIFY,
        HOOPYGANG,
        KWAI,
        CRITEO,
        MEDIA_MATIC,
        CHANNEL_FACTORY,
        XAXIS,
        SPOTIFY,
    ),
    DISPLAY: (
        ADFORM,
        AMAZON,
        BAIDU,
        CAPTIFY,
        DISCOVERY_ADS,
        HYBRID_AI,
        MEDIA_MATH,
        TENCENT,
        THE_TRADE_DESK,
        VERIZON_MEDIA,
        WIDESPACE,
        XANDR,
        SIZMEK,
        CRITEO,
        QUANTCAST,
        GOOGLE_DISPLAY_NETWORK_EXCLUDED,
        TEADS,
        AWIN,
        BLUE,
        BMC,
        MAIN_AD,
        UNIVERSAL_APP_CAMPAIGN,
        TABRIS,
        TRADE_DOUBLER,
        REFINE,
        MEDIA_MATIC,
        MEDIATIQUE,
        HIC_MOBILE,
        XAXIS,
        AVRAGE_MEDIA,
        ACCUEN,
        ADPLAY,
        ZETA_DSP,
        ZETA_EIKON,
        SOGOU,
        RAKUTEN_ADV,
        BUZZOOLE,
        S4M,
        HOOPYGANG,
        YANDEX,
        INSTAL,
        EDISCOM,
        SPOTIFY,
        INVIBES,
        YOUTUBE,
        DOVE_CONVIENE,
        OVERVIEW_TD,
        ONETAG,
        INCUBETA,
    ),
    VIDEO: (
        ADFORM,
        ADOBE_DSP_TUBEMOGUL,
        AMAZON,
        CAPTIFY,
        FACEBOOK,
        DV360_EXCLUDED,
        MEDIA_MATH,
        SMARTCLIP,
        SPOTIFY,
        SPOTX,
        TEADS,
        THE_TRADE_DESK,
        TIK_TOK,
        VERIZON_MEDIA,
        YOUTUBE,
        CRITEO,
        TABRIS,
        CHANNEL_FACTORY,
        OUTBRAIN,
        PINTEREST,
        XAXIS,
        ADPLAY,
        ACCUEN,
        ZETA_EIKON,
        SOGOU,
        RAKUTEN_ADV,
        BUZZOOLE,
        S4M,
        HOOPYGANG,
        XANDR,
        HYBRID_AI,
        WIDESPACE,
        ZETA_EIKON,
        ZEMANTA,
        YANDEX,
        LINKEDIN,
        REDDIT,
        TUMBLR,
        TWITTER,
        VK_COM,
        TWITCH,
        XING,
        INSTAL,
        INSTAGRAM,
        GOOGLE_DISPLAY_NETWORK_EXCLUDED,
        GOOGLE_DISPLAY_NETWORK_VIRTUAL,
        MAIN_AD,
        TWITCH,
        SNAP_CHAT,
        INVIBES,
        DOVE_CONVIENE,
        OVERVIEW_TD,
        MYVIDEO_TARGETING,
        REFINE,
    ),
    NATIVE: (
        AD_YOU_LIKE,
        CAPTIFY,
        OUTBRAIN,
        QUANTCAST,
        TABOOLA,
        THE_TRADE_DESK,
        VERIZON_MEDIA,
        ZEMANTA,
        TEADS,
        AMAZON,
        TRADE_DOUBLER,
        XAXIS,
        ADPLAY,
        REFINE,
        CRITEO,
        ACCUEN,
        ZETA_DSP,
        ZETA_EIKON,
        RAKUTEN_ADV,
        XANDR,
        MEDIA_MATH,
        WIDESPACE,
        ZETA_EIKON,
        DV360_EXCLUDED,
        DV360_VIRTUAL,
        INSTAL,
        RANKIT,
        EDISCOM,
        INVIBES,
        BING,
        DOVE_CONVIENE,
    ),
    MOBILE: (
        HIC_MOBILE,
        INSTAL,
        OGURY,
        WIDESPACE,
        CAPTIFY,
        WAZE,
        OUTBRAIN,
        PSAFE,
        ACCUEN,
        ZETA_EIKON,
        RAKUTEN_ADV,
        S4M,
        FACEBOOK,
        XANDR,
        ADFORM,
        HYBRID_AI,
        MEDIA_MATH,
        ZETA_EIKON,
        DV360_EXCLUDED,
        DV360_VIRTUAL,
        ZEMANTA,
        YANDEX,
        CRITEO,
        GOOGLE_DISPLAY_NETWORK_EXCLUDED,
        GOOGLE_DISPLAY_NETWORK_VIRTUAL,
        MAIN_AD,
        DOVE_CONVIENE,
        OVERVIEW_TD,
    ),
    DEM: (
        REFINE,
        TRADE_DOUBLER,
        DB_SHAPER,
        ADVERTISME,
        ANTEVENIO,
        ARKEERO,
        BMC,
        CLICKPOINT,
        EDISCOM,
        GEKO,
        KETCHUP,
        PAYCLICK,
        PERFORMAGENCY,
        PUREBROS,
        DIGITAL_VENTURE,
        AVRAGE_MEDIA,
        LINKAPPEAL,
        PUBLY,
        TALKS_MEDIA,
        VERTIGO,
        ADVICE_ME,
        TOP_PARTNERS,
        ADPLAY,
        IGOAL,
        XAXIS,
        HIC_MOBILE,
        DOVE_CONVIENE,
    ),
    PROGRAMMATIC_RADIO: (
        ADFORM,
        ADS_WIZZ,
        AMAZON,
        DEEZER,
        DV360_EXCLUDED,
        SPOTIFY,
        THE_TRADE_DESK,
        TENCENT_MUSIC_SPOTIFY,
        CAPTIFY,
        ACCUEN,
        S4M,
        MEDIA_MATIC,
        XAXIS,
        DV360_VIRTUAL,
    ),
    DOOH: (
        AD_QUICK_COM,
        DV360_EXCLUDED,
        MEDIA_MATH,
        TAGGIFY,
        VISTAR_MEDIA,
        ADFORM,
        S4M,
        DV360_VIRTUAL,
        DOVE_CONVIENE,
        OVERVIEW_TD,
    ),
    ADDRESSABLE_TV: (
        AD_YOU_LIKE,
        FACEBOOK,
        GOOGLE_YOUTUBE_CTV,
        SMARTCLIP,
        XANDR,
        SAMSUNG_ADS,
        MEDIA_MATH,
        RAKUTEN_ADV,
        YOUTUBE,
        AMAZON,
    ),
    CHATBOT: (
        HEJ,
        SPIN_UP,
        EDISCOM,
    ),
    OFFLINE_TV: (
        RAI,
        LA7,
        PUBLITALIA,
    ),
    BRANDED_CONTENT: (
        AMAZON,
        ANTEVENIO,
        REFINE,
    ),
    INFLUENCER_MARKETING: (
        ANTEVENIO,
        RANKIT,
    ),
}

INVALID_NOT_INTEGRATED_PLATFORMS = [
    PUBLY,
    PAYCLICK,
    CHILI,
    ADVICE_ME,
    ZETA_DSP,
    OUTBRAIN,
    TENCENT_MUSIC_SPOTIFY,
    SPOTX,
    DEEZER,
    AWIN,
    BLUE,
    ADVERTISME,
    AVRAGE_MEDIA,
    LINKAPPEAL,
    TALKS_MEDIA,
    VERTIGO,
    WAZE,
    TOP_PARTNERS,
    MEDIATIQUE,
    SPIN_UP,
    ADPLAY,
    DV360_EXCLUDED,
    GOOGLE_YOUTUBE_CTV,
    GOOGLE_DISPLAY_NETWORK_EXCLUDED,
]

NOT_INTEGRATED_PLATFORMS_WITHOUT_INVALID = {
    platform: label for platform, label in NOT_INTEGRATED_PLATFORMS.items()
    if platform not in INVALID_NOT_INTEGRATED_PLATFORMS
}

CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_WITHOUT_INVALID = {
    channel: [platform for platform in platforms if platform not in INVALID_NOT_INTEGRATED_PLATFORMS]
    for channel, platforms in CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_MAP.items()
}
