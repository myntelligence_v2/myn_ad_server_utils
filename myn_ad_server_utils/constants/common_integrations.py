# Special cases that can be in any list (Platforms, Ad Network, Media, Ad server)
SPOTIFY = 'spotify'
ADFORM = 'adform'
SIZMEK = 'sizmek'
CAPTIFY = 'captify'
PUBLY = 'publy'
TALKS_MEDIA = 'talks_media'
KETCHUP = 'ketchup'
EDISCOM = 'ediscom'
ACCUEN = 'accuen'
CHILI = 'chili'
ALIBABA = 'alibaba'
PAYCLICK = 'payclick'
ADVICE_ME = "advice_me"
SOGOU = 'sogou'
RAKUTEN_ADV = 'rakuten_adv'
ARENA_DAEMON = 'arena_daemon'
BUZZOOLE = 'buzzoole'
BEELEADS = 'beeleads'
LIVESPORT_MEDIA = 'livesport_media'
IGOAL = 'igoal'
S4M = 's_four_m'
MOTORK = 'motork'
RTL_ADCONNECT = 'rtl_adconnect'
HOOPYGANG = 'hoopygang'
EDIMOTIVE = 'edimotive'
OUTBRAIN = 'outbrain'
TENCENT_MUSIC_SPOTIFY = 'tencent_music_spotify'
FONDAZIONE_MARIA_BELLISARIO = 'fondazione_maria_bellisario'
SPOTX = 'spotx'
DEEZER = 'deezer'
AWIN = 'awin'
BLUE = 'blue'
ADVERTISME = 'advertisme'
AVRAGE_MEDIA = 'avrage_media'
LINKAPPEAL = 'linkappeal'
VERTIGO = 'vertigo'
WAZE = 'waze'
TOP_PARTNERS = 'top_partners'
MEDIATIQUE = 'mediatique'
SPIN_UP = 'spin_up'
ADPLAY = 'adplay'
INVIBES = 'invibes'
RCS = 'rcs'
DOVE_CONVIENE = 'dove_conviene'
OVERVIEW_TD = 'overview_td'

COMMON_INTEGRATIONS = {
    SPOTIFY: 'Spotify',
    ADFORM: 'AdForm DSP',
    SIZMEK: 'Sizmek',
    CAPTIFY: 'Captify',
    PUBLY: 'Takerate',
    TALKS_MEDIA: 'Talks Media',
    KETCHUP: 'KetchUP',
    EDISCOM: 'Ediscom DSP',
    ACCUEN: 'Accuen',
    CHILI: 'Chili Media',
    PAYCLICK: 'PayClick',
    ALIBABA: 'Alibaba',
    ADVICE_ME: "AdviceMe",
    SOGOU: 'Sogou',
    RAKUTEN_ADV: 'Rakuten ADV',
    ARENA_DAEMON: 'Arena Daemon',
    BUZZOOLE: 'BUZZOOLE',
    BEELEADS: 'BeeLeads',
    LIVESPORT_MEDIA: 'Livesport Media',
    IGOAL: 'iGoal',
    S4M: 'S4M',
    MOTORK: 'MOTORK',
    RTL_ADCONNECT: 'RTL AdConnect',
    HOOPYGANG: 'Hoopygang',
    EDIMOTIVE: 'Edimotive',
    OUTBRAIN: 'Outbrain',
    TENCENT_MUSIC_SPOTIFY: 'Tencent Music Entertainment',
    FONDAZIONE_MARIA_BELLISARIO: 'Fondazione Maria Bellisario',
    SPOTX: 'SpotX',
    DEEZER: 'Deezer',
    AWIN: 'AWIN',
    BLUE: 'Blue Ads Media',
    ADVERTISME: 'Advertisme',
    AVRAGE_MEDIA: 'Avrage Media',
    LINKAPPEAL: 'Linkappeal',
    VERTIGO: 'Vertigo',
    WAZE: 'Waze Ads',
    TOP_PARTNERS: 'Top Partners',
    MEDIATIQUE: 'Médiatique',
    SPIN_UP: 'Spinup',
    ADPLAY: 'AdPlay',
    INVIBES: 'Invibes',
    RCS: 'CAIRORCS MEDIA',
    DOVE_CONVIENE: 'Dove Conviene',
    OVERVIEW_TD: 'Overview td',
}
