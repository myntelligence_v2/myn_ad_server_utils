from myn_ad_server_utils.constants.common_integrations import (
    CAPTIFY, EDISCOM,
    KETCHUP, ACCUEN, PUBLY,
    TALKS_MEDIA,
    COMMON_INTEGRATIONS,
    PAYCLICK, ADVICE_ME,
    ARENA_DAEMON,
    BEELEADS,
    LIVESPORT_MEDIA,
    MOTORK,
    RTL_ADCONNECT,
    EDIMOTIVE,
    OUTBRAIN,
    SPOTX,
    AWIN,
    BLUE,
    AVRAGE_MEDIA,
    LINKAPPEAL,
    TOP_PARTNERS,
    MEDIATIQUE,
    SPIN_UP,
    INVIBES,
    DOVE_CONVIENE,
    OVERVIEW_TD,
)
from myn_ad_server_utils.constants.channels import (
    DISPLAY, VIDEO, NATIVE,
    MOBILE, DEM, DOOH,
    PROGRAMMATIC_RADIO, CHATBOT,
    SOCIAL, SEARCH, ADDRESSABLE_TV, BRANDED_CONTENT, INFLUENCER_MARKETING,
)
from myn_ad_server_utils.constants.invalid_media_ad_networks import INVALID_MEDIA_AD_NETWORKS

FOUR_W_MARKET_PLACE = 'four_w_market_place'
AD_KAORA = 'ad_kaora'
BEINTOO = 'beintoo'
BLIS = 'blis'
EVOLUTION_ADV = 'evolution_adv'
GOOD_MOVE = 'good_move'
MOVING_UP = 'moving_up'
NET_MEDIA_CLICK = 'net_media_click'
SEEDTAG = 'seedtag'
WEB_ADS = 'web_ads'
BROADSIGN = 'broadsign'
EXTERION_MEDIA = 'exterion_media'
JCDECAUX_VIOOH = 'jcdecaux_viooh'
OOH = 'ooh'
NATIVO = 'nativo'
BITDROME = 'bitdrome'
COGNITIVE = 'cognitive'
FANPLAYR = 'fanplayr'
MEDIAMOB = 'mediamob'
LOGAN = 'logan'
TIMWE_TECH = 'timwe_tech'
INFINIA = 'infinia'
AFILIO = 'afilio'
SUBLIME = 'sublime'
INDEED = 'indeed'
DIGITAL_POINT = 'digital_point'
BIEFFE = "bieffe"
LEADMEDIA = "leadmedia"
YBOX = "ybox"
AZERION = 'azerion'
QUALIFY_DIRECT = 'qualify_direct'
POP_ADVERTISING = 'pop_advertising'
BREAK_EVEN = 'break_even'
DIGITAL_BLOOM = 'digital_bloom'
BLIS_MEDIA_LIMITED = 'blis_media_limited'
VALICA = 'valica'
LUCKY_ADV = 'lucky_adv'
MEDIACUBE = 'mediacube'
E_BUSINESS_CONSULTING = 'e_business_consulting'
KWANKO = 'kwanko'
IMMAKERS = 'immakers'
TIGERMEDIA = 'tigermedia'
DGMAX = 'dgmax'
ROCKETLAB = 'rocketlab'
RANKMYAPP = 'rankmyapp'
WEACH_GROUP = 'weach_group'
GDB_GLOABL_DATA_BANK = 'gdb_global_data_bank'
CISNEROS = 'cisneros'
OPL_DIGITAL = 'opl_digital'
HYPR = 'hypr'
MYACOM = 'myacom'
NETMEDIA = 'netmedia'
APEX = 'apex'
ENDOXOS = 'endoxos'
IDA_AGENCY = 'ida_agency'
E_LEADS = 'e_leads'
OPTIMISE = 'optimise'
ANUNCIAR_MIDIA = 'anunciar_midia'
PERFORMONEY = 'performoney'
FINANCE_ADS = 'finance_ads'
ADS_PLAY = 'adsplay'
SEVENDATA = 'sevendata'
MIA_MOBILE = 'mia_mobile'
BUNKER_ADS = 'bunker_ads'
PERFORM_UP = 'perform_up'
ACROSS= 'across'
PMP = "pmp"
SILVER_BULLET = "silver_bullet"
VIRALIZE = "viralize"


AD_NETWORK_INTEGRATIONS = {
    FOUR_W_MARKET_PLACE: '4w MarketPlace',
    AD_KAORA: 'AdKaora',
    BEINTOO: 'Beintoo',
    BLIS: 'Blis',
    EVOLUTION_ADV: 'Evolution Adv',
    GOOD_MOVE: 'Good Move',
    MOVING_UP: 'Moving Up',
    NET_MEDIA_CLICK: 'nmc Netmediaclick',
    PUBLY: COMMON_INTEGRATIONS[PUBLY],
    SEEDTAG: 'Seedtag',
    TALKS_MEDIA: COMMON_INTEGRATIONS[TALKS_MEDIA],
    WEB_ADS: 'WebAds',
    BROADSIGN: 'Broadsign',
    EXTERION_MEDIA: 'Exterion Media',
    JCDECAUX_VIOOH: 'JCDecaux',
    OOH: 'OOH!',
    NATIVO: 'NATIVO',
    BITDROME: 'Bitdrome',
    DOVE_CONVIENE: COMMON_INTEGRATIONS[DOVE_CONVIENE],
    CAPTIFY: COMMON_INTEGRATIONS[CAPTIFY],
    COGNITIVE: 'Cognitive',
    FANPLAYR: 'Fanplayr',
    MEDIAMOB: 'Mediamob',
    TIMWE_TECH: 'TIMWE Tech',
    INFINIA: 'Infinia Mobile',
    AFILIO: 'AFILIO',
    LOGAN: 'LOGAN',
    OVERVIEW_TD: COMMON_INTEGRATIONS[OVERVIEW_TD],
    SUBLIME: 'Sublime',
    INVIBES: COMMON_INTEGRATIONS[INVIBES],
    INDEED: 'Indeed',
    DIGITAL_POINT: 'Digital Point',
    BIEFFE: "Bieffe",
    LEADMEDIA: "LeadMedia",
    YBOX: "ybox",
    AZERION: 'Azerion',
    QUALIFY_DIRECT: 'Qualify Direct',
    POP_ADVERTISING: 'Pop Advertising',
    BREAK_EVEN: 'Break Even',
    KETCHUP: COMMON_INTEGRATIONS[KETCHUP],
    EDISCOM: COMMON_INTEGRATIONS[EDISCOM],
    ACCUEN: COMMON_INTEGRATIONS[ACCUEN],
    DIGITAL_BLOOM: 'Digital Bloom',
    BLIS_MEDIA_LIMITED: 'Blis Media Limited',
    VALICA: 'Valica',
    LUCKY_ADV: 'Lucky Adv',
    PAYCLICK: COMMON_INTEGRATIONS[PAYCLICK],
    MEDIACUBE: 'Mediacube',
    E_BUSINESS_CONSULTING: 'E-Business Consulting',
    KWANKO: 'Kwanko',
    IMMAKERS: 'IMMAKERS',
    TIGERMEDIA: 'Tigermedia',
    DGMAX: 'DGMAX',
    ROCKETLAB: 'Rocketlab',
    RANKMYAPP: 'Rankmyapp',
    WEACH_GROUP: 'Weach Group',
    GDB_GLOABL_DATA_BANK: 'GDB - Global Data Bank',
    CISNEROS: 'CISNEROS',
    OPL_DIGITAL: 'OPL DIGITAL',
    HYPR: 'HYPR',
    MYACOM: 'Myacom',
    NETMEDIA: 'netmedia',
    APEX: 'APEX',
    ENDOXOS: 'ENDOXOS',
    IDA_AGENCY: 'IDA Agency',
    ADVICE_ME: COMMON_INTEGRATIONS[ADVICE_ME],
    E_LEADS: 'E-Leads',
    ARENA_DAEMON: COMMON_INTEGRATIONS[ARENA_DAEMON],
    BEELEADS: COMMON_INTEGRATIONS[BEELEADS],
    LIVESPORT_MEDIA: COMMON_INTEGRATIONS[LIVESPORT_MEDIA],
    MOTORK: COMMON_INTEGRATIONS[MOTORK],
    RTL_ADCONNECT: COMMON_INTEGRATIONS[RTL_ADCONNECT],
    EDIMOTIVE: COMMON_INTEGRATIONS[EDIMOTIVE],
    OUTBRAIN: COMMON_INTEGRATIONS[OUTBRAIN],
    OPTIMISE: 'Optimise',
    ANUNCIAR_MIDIA: 'ANUNCIAR MÍDIA',
    SPOTX: COMMON_INTEGRATIONS[SPOTX],
    AWIN: COMMON_INTEGRATIONS[AWIN],
    BLUE: COMMON_INTEGRATIONS[BLUE],
    AVRAGE_MEDIA: COMMON_INTEGRATIONS[AVRAGE_MEDIA],
    LINKAPPEAL: COMMON_INTEGRATIONS[LINKAPPEAL],
    TOP_PARTNERS: COMMON_INTEGRATIONS[TOP_PARTNERS],
    MEDIATIQUE: COMMON_INTEGRATIONS[MEDIATIQUE],
    SPIN_UP: COMMON_INTEGRATIONS[SPIN_UP],
    PERFORMONEY: 'Performoney',
    FINANCE_ADS: 'financeAds',
    ADS_PLAY: 'AdsPlay',
    SEVENDATA: 'SEVENDATA',
    MIA_MOBILE: 'Mia Mobile',
    BUNKER_ADS: 'BunkerAds',
    PERFORM_UP: 'PerformUP',
    ACROSS: 'Across',
    PMP: "PMP",
    SILVER_BULLET: "Silver Bullet",
    VIRALIZE: "Viralize",
}

CHANNELS_TO_AD_NETWORK_MAP = {
    SEARCH: (
        PAYCLICK,
        APEX,
        MOTORK,
        RTL_ADCONNECT,
        AWIN,
        TOP_PARTNERS,
        MEDIATIQUE,
        BIEFFE,
        COGNITIVE,
        FANPLAYR,
    ),
    SOCIAL: (
        PUBLY,
        PAYCLICK,
        NETMEDIA,
        APEX,
        IDA_AGENCY,
        ADVICE_ME,
        MOTORK,
        RTL_ADCONNECT,
        OUTBRAIN,
        TOP_PARTNERS,
        MEDIATIQUE,
        AD_KAORA,
        COGNITIVE,
        FANPLAYR,
    ),
    DISPLAY: (
        FOUR_W_MARKET_PLACE,
        AD_KAORA,
        BLIS,
        EVOLUTION_ADV,
        GOOD_MOVE,
        MOVING_UP,
        NET_MEDIA_CLICK,
        PUBLY,
        SEEDTAG,
        TALKS_MEDIA,
        WEB_ADS,
        CAPTIFY,
        DOVE_CONVIENE,
        COGNITIVE,
        FANPLAYR,
        BITDROME,
        MEDIAMOB,
        AFILIO,
        OVERVIEW_TD,
        SUBLIME,
        INVIBES,
        YBOX,
        AZERION,
        POP_ADVERTISING,
        BREAK_EVEN,
        DIGITAL_BLOOM,
        VALICA,
        LUCKY_ADV,
        PAYCLICK,
        MEDIACUBE,
        QUALIFY_DIRECT,
        KWANKO,
        IMMAKERS,
        TIGERMEDIA,
        DGMAX,
        ROCKETLAB,
        RANKMYAPP,
        WEACH_GROUP,
        GDB_GLOABL_DATA_BANK,
        CISNEROS,
        OPL_DIGITAL,
        HYPR,
        MYACOM,
        NETMEDIA,
        APEX,
        ADVICE_ME,
        E_LEADS,
        ARENA_DAEMON,
        BEELEADS,
        LIVESPORT_MEDIA,
        MOTORK,
        RTL_ADCONNECT,
        EDIMOTIVE,
        OPTIMISE,
        ANUNCIAR_MIDIA,
        LEADMEDIA,
        AWIN,
        BLUE,
        AVRAGE_MEDIA,
        TOP_PARTNERS,
        MEDIATIQUE,
        SPIN_UP,
        BEINTOO,
        PERFORMONEY,
        FINANCE_ADS,
        LOGAN,
        BIEFFE,
        ADS_PLAY,
        PERFORM_UP,
        INFINIA,
        ACROSS,
        SILVER_BULLET,
        VIRALIZE,
    ),
    VIDEO: (
        FOUR_W_MARKET_PLACE,
        AD_KAORA,
        BITDROME,
        BLIS,
        GOOD_MOVE,
        MOVING_UP,
        NET_MEDIA_CLICK,
        SEEDTAG,
        TALKS_MEDIA,
        WEB_ADS,
        DOVE_CONVIENE,
        COGNITIVE,
        SUBLIME,
        ACCUEN,
        EVOLUTION_ADV,
        AZERION,
        PUBLY,
        PAYCLICK,
        NETMEDIA,
        APEX,
        ADVICE_ME,
        ARENA_DAEMON,
        MOTORK,
        RTL_ADCONNECT,
        OUTBRAIN,
        ANUNCIAR_MIDIA,
        CISNEROS,
        SPOTX,
        AVRAGE_MEDIA,
        TOP_PARTNERS,
        SPIN_UP,
        BEINTOO,
        LOGAN,
        VALICA,
        LUCKY_ADV,
        FANPLAYR,
        MEDIAMOB,
        INFINIA,
    ),
    NATIVE: (
        FOUR_W_MARKET_PLACE,
        NATIVO,
        PUBLY,
        SEEDTAG,
        DOVE_CONVIENE,
        EVOLUTION_ADV,
        GOOD_MOVE,
        BREAK_EVEN,
        WEB_ADS,
        KETCHUP,
        EDISCOM,
        DIGITAL_BLOOM,
        COGNITIVE,
        VALICA,
        LUCKY_ADV,
        PAYCLICK,
        MEDIACUBE,
        ACCUEN,
        QUALIFY_DIRECT,
        POP_ADVERTISING,
        AD_KAORA,
        AZERION,
        NETMEDIA,
        APEX,
        ADVICE_ME,
        NET_MEDIA_CLICK,
        MOTORK,
        RTL_ADCONNECT,
        OUTBRAIN,
        FANPLAYR,
        INFINIA,
        PMP,
    ),
    MOBILE: (
        DOVE_CONVIENE,
        BITDROME,
        AD_KAORA,
        MEDIAMOB,
        LOGAN,
        TIMWE_TECH,
        INFINIA,
        AZERION,
        PAYCLICK,
        APEX,
        BLIS,
        ADVICE_ME,
        ARENA_DAEMON,
        LIVESPORT_MEDIA,
        MOTORK,
        RTL_ADCONNECT,
        OUTBRAIN,
        AWIN,
        AVRAGE_MEDIA,
        TALKS_MEDIA,
        SPIN_UP,
        BEINTOO,
        SUBLIME,
        MIA_MOBILE,
    ),
    DEM: (
        MEDIAMOB,
        INDEED,
        DIGITAL_POINT,
        BIEFFE,
        LEADMEDIA,
        QUALIFY_DIRECT,
        BREAK_EVEN,
        E_BUSINESS_CONSULTING,
        PAYCLICK,
        PUBLY,
        COGNITIVE,
        APEX,
        MYACOM,
        NETMEDIA,
        POP_ADVERTISING,
        ENDOXOS,
        ADVICE_ME,
        NET_MEDIA_CLICK,
        MOTORK,
        RTL_ADCONNECT,
        ANUNCIAR_MIDIA,
        AVRAGE_MEDIA,
        LINKAPPEAL,
        TALKS_MEDIA,
        TOP_PARTNERS,
        AD_KAORA,
        WEB_ADS,
        MEDIACUBE,
        VALICA,
        LUCKY_ADV,
        SEVENDATA,
        SPIN_UP,
        BUNKER_ADS,
        FANPLAYR,
        AFILIO,
    ),
    DOOH: (
        BROADSIGN,
        EXTERION_MEDIA,
        JCDECAUX_VIOOH,
        OOH,
        APEX,
        NET_MEDIA_CLICK,
        LOGAN,
        FANPLAYR,
        MEDIAMOB,
    ),
    PROGRAMMATIC_RADIO: (
        MOVING_UP,
        BLIS_MEDIA_LIMITED,
        APEX,
        NET_MEDIA_CLICK,
        MOTORK,
        RTL_ADCONNECT,
        FOUR_W_MARKET_PLACE,
        COGNITIVE,
        BROADSIGN,
    ),
    CHATBOT: (
        EDISCOM,
        APEX,
        SPIN_UP,
        E_LEADS,
    ),
    ADDRESSABLE_TV: (
        MOTORK,
        RTL_ADCONNECT,
        LOGAN,
    ),
    BRANDED_CONTENT: (
        EVOLUTION_ADV,
    ),
    INFLUENCER_MARKETING: (
        IDA_AGENCY,
    )
}

AD_NETWORK_INTEGRATIONS_WITHOUT_INVALID = {
    ad_network: label for ad_network, label in AD_NETWORK_INTEGRATIONS.items()
    if ad_network not in INVALID_MEDIA_AD_NETWORKS
}

CHANNELS_TO_AD_NETWORKS_WITHOUT_INVALID = {
    channel: [ad_network for ad_network in ad_networks if ad_network not in INVALID_MEDIA_AD_NETWORKS]
    for channel, ad_networks in CHANNELS_TO_AD_NETWORK_MAP.items()
}
