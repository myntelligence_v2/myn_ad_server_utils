from collections import defaultdict
from myn_ad_server_utils.constants.channels import (
    SEARCH, SOCIAL, DISPLAY,
    VIDEO,
)


FACEBOOK = 'facebook'
DV360 = 'dv360_integrated'
ADWORDS = 'adwords'
THEOUTPLAY = 'theoutplay'
GOOGLE_DISPLAY_NETWORK = 'google_display_network_integrated'
YOUTUBE_DV360_INTEGRATED = 'youtube_dv360_integrated'
YOUTUBE_GOOGLE_ADS_INTEGRATED = 'youtube_google_ads_integrated'

INTEGRATED_PLATFORMS = {
    DV360: 'DV360',
    FACEBOOK: 'Facebook',
    THEOUTPLAY: 'MyVideo',
    ADWORDS: 'Google Ads',
    GOOGLE_DISPLAY_NETWORK: 'Google Display Network',
    YOUTUBE_DV360_INTEGRATED: 'Youtube (DV360)'
}

CHANNELS_TO_INTEGRATED_PLATFORMS_MAP = defaultdict(tuple)
CHANNELS_TO_INTEGRATED_PLATFORMS_MAP.update({
    SEARCH: (ADWORDS,),
    SOCIAL: (FACEBOOK,),
    DISPLAY: (DV360, GOOGLE_DISPLAY_NETWORK),
    VIDEO: (THEOUTPLAY, DV360, YOUTUBE_DV360_INTEGRATED),
})
