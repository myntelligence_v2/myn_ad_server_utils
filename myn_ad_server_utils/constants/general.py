from collections import defaultdict
from itertools import chain
from myn_ad_server_utils.constants.ad_network import *
from myn_ad_server_utils.constants.ad_servers import *
from myn_ad_server_utils.constants.audience import *
from myn_ad_server_utils.constants.channels import *
from myn_ad_server_utils.constants.common_integrations import *
from myn_ad_server_utils.constants.ghost_platforms import *
from myn_ad_server_utils.constants.integrated_platforms import *
from myn_ad_server_utils.constants.media import *
from myn_ad_server_utils.constants.web_analytics import *
from myn_ad_server_utils.constants.brand_safety import BRAND_SAFETY_INTEGRATIONS


INTEGRATED_TYPE = 'integrated'
FUTURE_INTEGRATION_TYPE = 'future_integration'
CUSTOM_TYPE = 'custom'

CHANNELS_NAME_MAP = {
    **INTEGRATED_CHANNELS,
    **NOT_INTEGRATED_CHANNELS
}


PLATFORMS_NAME_MAP = {
    **INTEGRATED_PLATFORMS,
    **NOT_INTEGRATED_PLATFORMS
}

#
# default metrics for tracking filters
#
DEFAULT_METRIC_NAMES = [
    'CPL',
    'CPA',
    'CPI',
    'CPS',
    'CPD'
]

DEFAULT_LABEL_NAMES = [
    'Leads',
    'Acquisitions',
    'Installs',
    'Sales',
    'Downloads',
]

MEDIA = 'publishers'
AD_NETWORK = 'ad_network'
AD_SERVER = 'ad_server'
BRAND_SAFETY = 'brand_safety'


CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_WITHOUT_INVALID_WITH_AUDIENCE = defaultdict(list)

for channel, platforms in chain(
    CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_WITHOUT_INVALID.items(),
    CHANNELS_TO_AUDIENCE_INTEGRATIONS_MAP.items(),
):
    CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_WITHOUT_INVALID_WITH_AUDIENCE[channel] += platforms

CHANNELS_NAME_MAP_WITH_AUDIENCE = {
    **CHANNELS_NAME_MAP,
    **AUDIENCE_CHANNELS_NAME_MAP,
}

AVAILABLE_INTEGRATIONS = defaultdict(list)

for channel, platforms in chain(
    CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_WITHOUT_INVALID.items(),
    CHANNELS_TO_INTEGRATED_PLATFORMS_MAP.items(),
    CHANNELS_TO_AUDIENCE_INTEGRATIONS_MAP.items(),
    {MEDIA: MEDIA_INTEGRATIONS_WITHOUT_INVALID}.items(),
    {AD_NETWORK: AD_NETWORK_INTEGRATIONS_WITHOUT_INVALID}.items(),
    {AD_SERVER: AD_SERVERS}.items(),
    {WEB_ANALYTICS: WEB_ANALYTICS_INTEGRATIONS}.items(),
    {BRAND_SAFETY: BRAND_SAFETY_INTEGRATIONS}.items(),
):
    AVAILABLE_INTEGRATIONS[channel] += platforms


PARTNERS = {
    **NOT_INTEGRATED_PLATFORMS,
    **MEDIA_INTEGRATIONS,
    **AD_NETWORK_INTEGRATIONS,
}
