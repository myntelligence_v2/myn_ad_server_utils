from myn_ad_server_utils.constants.common_integrations import (
    SPOTIFY,
    CHILI,
    COMMON_INTEGRATIONS,
    ALIBABA,
    SOGOU,
    RAKUTEN_ADV,
    BUZZOOLE,
    IGOAL,
    S4M,
    HOOPYGANG,
    TENCENT_MUSIC_SPOTIFY,
    FONDAZIONE_MARIA_BELLISARIO,
    DEEZER,
    ADVERTISME,
    VERTIGO,
    WAZE,
    ADPLAY,
    RCS,
)
from myn_ad_server_utils.constants.channels import (
    SEARCH, SOCIAL, DISPLAY,
    VIDEO, NATIVE, MOBILE,
    DEM, PROGRAMMATIC_RADIO, DOOH,
    ADDRESSABLE_TV, BRANDED_CONTENT, INFLUENCER_MARKETING
)
from myn_ad_server_utils.constants.invalid_media_ad_networks import INVALID_MEDIA_AD_NETWORKS


TRIBOO = 'triboo'
AL_FEMMINILE = 'al_femminile'
AUTOSCOUT = 'autoscout'
CIAOPEOPLE_ADV_FANPAGE = 'ciaopeople_adv_fanpage'
CITYNEWS = 'citynews'
CLASS_PUBBLICITA = 'class_pubblicita'
CONDENAST = 'condenast'
DAZN = 'dazn'
DISCOVERY_MEDIA = 'discovery_media'
EDREAMS_ODIGEO = 'edreams_odigeo'
EURONEWS = 'euronews'
INNOVATIVE_PUBLISHING = 'innovative_publishing'
ITALIA_ON_LINE = 'italia_on_line'
LASTMINUTE = 'lastminute'
LUNIFIN_SRL = 'lunifin_srl'
MANZONI = 'manzoni'
MEDIAMOND = 'mediamond'
MOBILE_DE = 'mobile_de'
NET_ADDICTION = 'net_addiction'
NEWSONLINE_IOL = 'newsonline_iol'
PIEMME_ONLINE = 'piemme_online'
SKY = 'sky'
SPEED = 'speed'
SPORT_NETWORK = 'sport_network'
SUBITO = 'subito'
VISIBILIA = 'visibilia'
WEB_SYSTEM_24 = 'web_system_24'
AOL = 'aol'
MAIL_RU = 'mail_ru'
RAI_PUBBLICITA = 'rai_pubblicita'
RCS_PUBBLICITA = 'rcs_pubblicita'
IMMOBILIARE = 'immobiliare'
DOMUS = 'domus'
EDU_NET = 'edu_net'
SOS_TARIFFE = 'sos_tariffe'
FACILE_IT = 'facile_it'
AUTOTRADER = 'autotrader'
CARGURUS = 'cargurus'
EDMUNDS = 'edmunds'
KBB = 'kbb'
DIGITAL360 = 'digital_360'
LIVE_QUIZ = 'live_quiz'
UNIVERSITY_BOX = 'university_box'
GLOBO_COM = 'globo_com'
UOL = 'uol'
SHOPFULLY = 'shopfully'
R7 = 'r_7'
AZIENDA_BANCA = 'azienda_banca'
BFC_MEDIA = 'bfc_media'
DUESSE = 'duesse'
DISNEY = 'disney'
ONE_FOOTBALL = 'one_football'
EBAY = 'ebay'
LA_MESCOLANZA = 'la_mescolanza'
MOONDO = 'moondo'
IMS = 'ims'
START_MAGAZINE = 'start_magazine'
IL_SECOLO_D_ITALIA = 'il_secolo_d_italia'
GLI_STARI_GENERALI = 'gli_stari_generali'
AMATE_SPONDE = 'amate_sponde'
CAPACITY = 'capacity'
COMMUNICATION_AFRICA = 'communication_africa'
KADIUM = 'kadium'
VALEMEDIA_SRL = 'valemedia_srl'
RTB_HOUSE = 'rtb_house'
LA_NOTIZIA = 'la_notizia'
THE_POST_INTERNAZIONALE = 'the_post_internazionale'
ART_TRIBUNE = 'art_tribune'
MONEY_DISPLAY = 'money_display'
DAVIDE_MAGGIO = 'davide_maggio'
BANCA_FORTE = 'banca_forte'
TRAVEL_ALLIANCE = 'travel_alliance'
MELHOR_ESCOLHA = "melhor_escolha"
AUTOBILD = 'autobild'
MAGGIOLI = 'maggioli'
AUTO_IT = 'auto_it'
AUTOMOBILE_IT = 'automobile_it'
CARWOW = 'carwow'
MEDIA_SOLUTIONS = 'media_solutions'
VIBRANT_MEDIA = 'vibrant_media'
MEDIA_IMPACT = 'media_impact'
SEVEN_ONE_MEDIA = 'seven_one_media'
WEBBDONE = 'webbdone'
LE_MONDE = 'le_monde'
LEQUIPE = 'lequipe'
LA_VANGUARDIA = 'la_vanguardia'
AUTOXY = 'autoxy'
CAVAL = 'caval'
CAR_AFFINITY = 'car_affinity'
DRIVEK = 'drivek'
DAICAR = 'daicar'
MANEKI = 'maneki'
CRM_SRL_AUTOMOTO_IT = 'crm_srl_automoto_it'
EUROMEDIA = 'euromedia'
INDUSTRIA_ITALIANA = 'industria_italiana'
MOTORIONLINE = 'motorionline'
KOMPAROTORE = 'komparatore'
BANDA_B = 'banda_b'
GAZETA_DO_POVO = 'gazeta_do_povo'
TRIBUNA = 'tribuna'
DIARIO_DOS_CAMPOS = 'diario_dos_campos'
GMC_ONLINE = 'gmc_online'
O_BONDE = 'o_bonde'
GAUCHA_ZH = 'gaucha_zh'
NSC_TOTAL = 'nsc_total'
ND = 'nd'
O_POVO = 'o_povo'
IBAHIA = 'ibahia'
ROBERTA_JUNGMANN = 'roberta_jungmann'
ZAP_IMOVEIS = 'zap_imoveis'
MERCADO_LIVRE_MELI = 'mercado_livre_meli'
BAKECA = 'bakeca'
PAYTIPPER = 'paytipper'
TIENDEO = 'tiendeo'
GMAIL = 'gmail'
AMADEUS = 'amadeus'
RTL = 'rtl'
RDS = 'rds'
ADSALSA = 'adsalsa'
FOLHA_DE_PERNAMBUCO = 'folha_de_pernambuco'
PORTAL_CORREIO = 'portal_correio'
AGORA_RN = 'agora_rn'
DAHAS = 'dahas'
MELHOR_PLANO = 'melhor_plano'
NZN = 'nzn'
DIGITOO = 'digitoo'
MUTUI_ONLINE = 'mutui_online'
SEGUGIO_IT = 'segugio_it'
PANDORA = 'pandora'
ANSA = 'ansa'
LE_FONTI = 'le_fonti'
FOLHA_DE_PAULO = 'folha_de_paulo'
VOZ_DAS_COMUNIDADES = "voz_das_comunidades"
COMPARA_PLANO = "compara_plano"
QUATTRORUOTE = "quattroruote"
AL_VOLANTE = "al_volante"
MOTOR1 = "motor1"
NEWSAUTO = "newsauto"
CARPLANNER = "carplanner"


MEDIA_INTEGRATIONS = {
    TRIBOO: 'TRIBOO',
    AL_FEMMINILE: 'alfemminile',
    ALIBABA: COMMON_INTEGRATIONS[ALIBABA],
    AUTOSCOUT: 'AutoScout24',
    CIAOPEOPLE_ADV_FANPAGE: 'Ciaopeople',
    CITYNEWS: 'Citynews',
    CLASS_PUBBLICITA: 'Classpubblicità',
    CONDENAST: 'Condé Nast',
    DAZN: 'DAZN',
    DISCOVERY_MEDIA: 'Discovery MEDIA',
    EDREAMS_ODIGEO: 'eDreams ODIGEO',
    EURONEWS: 'Euronews',
    INNOVATIVE_PUBLISHING: 'Innovative Publishing',
    ITALIA_ON_LINE: 'italiaonline',
    LASTMINUTE: 'Lastminute',
    LUNIFIN_SRL: 'Lunifin Srl',
    MANZONI: 'Manzoni',
    MEDIAMOND: 'MEDIAMOND',
    MOBILE_DE: 'Mobile.de',
    NET_ADDICTION: 'NetAddiction',
    NEWSONLINE_IOL: 'NEWSONLINE',
    PIEMME_ONLINE: 'Piemme Online',
    SKY: 'SKY',
    SPEED: 'SpeeD',
    SPORT_NETWORK: 'Sport Network',
    SUBITO: 'Subito',
    VISIBILIA: 'Visibilia',
    WEB_SYSTEM_24: 'WebSystem 24',
    AOL: 'Aol.',
    MAIL_RU: 'Mail.Ru',
    SOGOU: COMMON_INTEGRATIONS[SOGOU],
    RAI_PUBBLICITA: 'Rai Pubblicità',
    RAKUTEN_ADV: COMMON_INTEGRATIONS[RAKUTEN_ADV],
    RCS_PUBBLICITA: 'RCS Pubblicità',
    SPOTIFY: COMMON_INTEGRATIONS[SPOTIFY],
    IMMOBILIARE: 'Immobiliare',
    DOMUS: 'Domus',
    EDU_NET: 'ENDU.NET',
    SOS_TARIFFE: 'SOStariffe',
    FACILE_IT: 'Facile.it',
    AUTOTRADER: 'Autotrader',
    CARGURUS: 'CarGurus',
    EDMUNDS: 'Edmunds',
    KBB: 'KBB',
    DIGITAL360: 'DIGITAL360',
    LIVE_QUIZ: 'Live Quiz',
    UNIVERSITY_BOX: 'University Box',
    GLOBO_COM: 'globo.com',
    UOL: 'UOL',
    SHOPFULLY: 'ShopFully',
    R7: 'R7',
    AZIENDA_BANCA: 'Azienda Banca',
    BFC_MEDIA: 'BFC Media',
    DUESSE: 'Duesse',
    DISNEY: 'Disney',
    ONE_FOOTBALL: 'OneFootball',
    EBAY: 'Ebay',
    LA_MESCOLANZA: 'La Mescolanza',
    MOONDO: 'Moondo',
    IMS: 'IMS',
    START_MAGAZINE: 'Start Magazine',
    IL_SECOLO_D_ITALIA: "Secolo d'Italia",
    GLI_STARI_GENERALI: 'Gli Stati Generali',
    AMATE_SPONDE: 'Amate Sponde',
    BUZZOOLE: COMMON_INTEGRATIONS[BUZZOOLE],
    CAPACITY: 'Capacity',
    COMMUNICATION_AFRICA: 'Communication Africa',
    KADIUM: 'Kadium',
    VALEMEDIA_SRL: 'Valemedia Srl',
    RTB_HOUSE: 'RTB House',
    LA_NOTIZIA: 'La Notizia',
    THE_POST_INTERNAZIONALE: 'The Post Internazionale',
    ART_TRIBUNE: 'Artribune',
    MONEY_DISPLAY: 'Money.it',
    DAVIDE_MAGGIO: 'Davide Maggio',
    BANCA_FORTE: 'Bancaforte',
    TRAVEL_ALLIANCE: 'The Travel Alliance',
    IGOAL: COMMON_INTEGRATIONS[IGOAL],
    MELHOR_ESCOLHA: 'Melhor Escolha',
    AUTOBILD: 'AutoBild',
    S4M: COMMON_INTEGRATIONS[S4M],
    MAGGIOLI: 'Maggioli',
    AUTO_IT: 'auto.it',
    AUTOMOBILE_IT: 'automobile.it',
    CARWOW: 'carwow',
    MEDIA_SOLUTIONS: 'Media Solutions',
    VIBRANT_MEDIA: 'VIBRANT',
    MEDIA_IMPACT: 'media impact',
    SEVEN_ONE_MEDIA: 'seven.one MEDIA',
    RCS: COMMON_INTEGRATIONS[RCS],
    WEBBDONE: 'WebbDone',
    HOOPYGANG: COMMON_INTEGRATIONS[HOOPYGANG],
    LE_MONDE: 'Le Monde',
    LEQUIPE: "L'Equipe",
    LA_VANGUARDIA: 'La Vanguardia',
    AUTOXY: 'AutoXY',
    CAVAL: 'CAVAL',
    CAR_AFFINITY: 'carAffinity',
    DRIVEK: 'DriveK',
    DAICAR: 'Daicar',
    MANEKI: 'Maneki',
    CRM_SRL_AUTOMOTO_IT: 'Automoto',
    EUROMEDIA: 'Euromedia',
    INDUSTRIA_ITALIANA: 'Industria Italiana',
    MOTORIONLINE: 'Motorionline',
    CHILI: COMMON_INTEGRATIONS[CHILI],
    KOMPAROTORE: 'Komparatore',
    BANDA_B: 'Banda B',
    GAZETA_DO_POVO: 'Gazeta do Povo',
    TRIBUNA: 'Tribuna',
    DIARIO_DOS_CAMPOS: 'Diário dos Campos (DCmais)',
    GMC_ONLINE: 'GMC Online',
    O_BONDE: 'O Bonde',
    GAUCHA_ZH: 'Gaúcha ZH (GZH)',
    NSC_TOTAL: 'NSC Total',
    ND: 'ND',
    O_POVO: 'O Povo',
    IBAHIA: 'iBahia',
    ROBERTA_JUNGMANN: 'Roberta Jungmann',
    ZAP_IMOVEIS: 'Zap Imóveis',
    MERCADO_LIVRE_MELI: 'MERCADO LIVRE - MELI',
    BAKECA: 'Bakeca',
    PAYTIPPER: 'PAYTIPPER',
    TIENDEO: 'tiendeo',
    GMAIL: 'Gmail',
    AMADEUS: 'Amadeus',
    RTL: 'RTL',
    RDS: 'RDS',
    ADSALSA: 'adSalsa',
    TENCENT_MUSIC_SPOTIFY: COMMON_INTEGRATIONS[TENCENT_MUSIC_SPOTIFY],
    DEEZER: COMMON_INTEGRATIONS[DEEZER],
    ADVERTISME: COMMON_INTEGRATIONS[ADVERTISME],
    VERTIGO: COMMON_INTEGRATIONS[VERTIGO],
    WAZE: COMMON_INTEGRATIONS[WAZE],
    ADPLAY: COMMON_INTEGRATIONS[ADPLAY],
    FONDAZIONE_MARIA_BELLISARIO: COMMON_INTEGRATIONS[FONDAZIONE_MARIA_BELLISARIO],
    FOLHA_DE_PERNAMBUCO: 'FOLHA de PERNAMBUCO',
    PORTAL_CORREIO: 'Portal Correio',
    AGORA_RN: 'Agora RN',
    DAHAS: 'DAHAS',
    MELHOR_PLANO: 'Melhor Plano',
    NZN: 'NZN',
    DIGITOO: 'Digitoo',
    MUTUI_ONLINE: 'MutuiOnline',
    PANDORA: 'Pandora',
    SEGUGIO_IT: 'Segugio.it',
    ANSA: 'Ansa',
    LE_FONTI: 'Le Fonti',
    FOLHA_DE_PAULO: 'Folha de S.Paulo',
    VOZ_DAS_COMUNIDADES: "Voz das Comunidades",
    COMPARA_PLANO: "Growth",
    QUATTRORUOTE: 'Quattroruote',
    AL_VOLANTE: "Al Volante",
    MOTOR1: "Motor1",
    NEWSAUTO: "Newsauto",
    CARPLANNER: "Carplanner",
}

CHANNELS_TO_MEDIA_MAP = {
    SEARCH: (
        AOL,
        MAIL_RU,
        SOGOU,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        TRIBOO,
        CARGURUS,
        EBAY,
    ),
    SOCIAL: (
        DISNEY,
        IMS,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        HOOPYGANG,
        LASTMINUTE,
        CHILI,
        TRIBOO,
        ITALIA_ON_LINE,
        PIEMME_ONLINE,
        SPORT_NETWORK,
    ),
    DISPLAY: (
        AL_FEMMINILE,
        ALIBABA,
        AUTOSCOUT,
        CIAOPEOPLE_ADV_FANPAGE,
        CITYNEWS,
        CONDENAST,
        DAZN,
        DISCOVERY_MEDIA,
        EDREAMS_ODIGEO,
        EURONEWS,
        INNOVATIVE_PUBLISHING,
        ITALIA_ON_LINE,
        LASTMINUTE,
        LUNIFIN_SRL,
        MANZONI,
        MEDIAMOND,
        MOBILE_DE,
        NET_ADDICTION,
        NEWSONLINE_IOL,
        PIEMME_ONLINE,
        SKY,
        SPEED,
        SPORT_NETWORK,
        SUBITO,
        TRIBOO,
        VISIBILIA,
        WEB_SYSTEM_24,
        SPOTIFY,
        AOL,
        IMMOBILIARE,
        MAIL_RU,
        RAI_PUBBLICITA,
        RAKUTEN_ADV,
        RCS_PUBBLICITA,
        SOGOU,
        DOMUS,
        EDU_NET,
        SOS_TARIFFE,
        FACILE_IT,
        AUTOTRADER,
        CARGURUS,
        EDMUNDS,
        KBB,
        DIGITAL360,
        LIVE_QUIZ,
        UNIVERSITY_BOX,
        GLOBO_COM,
        UOL,
        R7,
        SHOPFULLY,
        AZIENDA_BANCA,
        BFC_MEDIA,
        DUESSE,
        ONE_FOOTBALL,
        EBAY,
        LA_MESCOLANZA,
        MOONDO,
        IMS,
        START_MAGAZINE,
        IL_SECOLO_D_ITALIA,
        GLI_STARI_GENERALI,
        AMATE_SPONDE,
        BUZZOOLE,
        CAPACITY,
        COMMUNICATION_AFRICA,
        KADIUM,
        VALEMEDIA_SRL,
        RTB_HOUSE,
        LA_NOTIZIA,
        THE_POST_INTERNAZIONALE,
        ART_TRIBUNE,
        MONEY_DISPLAY,
        DAVIDE_MAGGIO,
        BANCA_FORTE,
        TRAVEL_ALLIANCE,
        AUTOBILD,
        S4M,
        MAGGIOLI,
        AUTO_IT,
        AUTOMOBILE_IT,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        HOOPYGANG,
        LE_MONDE,
        LEQUIPE,
        LA_VANGUARDIA,
        AUTOXY,
        CAVAL,
        CAR_AFFINITY,
        DRIVEK,
        DAICAR,
        CRM_SRL_AUTOMOTO_IT,
        EUROMEDIA,
        INDUSTRIA_ITALIANA,
        MOTORIONLINE,
        MANEKI,
        KOMPAROTORE,
        BANDA_B,
        GAZETA_DO_POVO,
        TRIBUNA,
        DIARIO_DOS_CAMPOS,
        GMC_ONLINE,
        O_BONDE,
        GAUCHA_ZH,
        NSC_TOTAL,
        ND,
        O_POVO,
        IBAHIA,
        ROBERTA_JUNGMANN,
        ZAP_IMOVEIS,
        MERCADO_LIVRE_MELI,
        CHILI,
        BAKECA,
        PAYTIPPER,
        TIENDEO,
        GMAIL,
        CLASS_PUBBLICITA,
        AMADEUS,
        FONDAZIONE_MARIA_BELLISARIO,
        DEEZER,
        ADPLAY,
        FOLHA_DE_PERNAMBUCO,
        PORTAL_CORREIO,
        AGORA_RN,
        DAHAS,
        MELHOR_PLANO,
        NZN,
        WAZE,
        MUTUI_ONLINE,
        SEGUGIO_IT,
        ANSA,
        LE_FONTI,
        FOLHA_DE_PAULO,
        QUATTRORUOTE,
        AL_VOLANTE,
        MOTOR1,
        NEWSAUTO,
        CARPLANNER,
    ),
    VIDEO: (
        AL_FEMMINILE,
        CIAOPEOPLE_ADV_FANPAGE,
        CITYNEWS,
        CLASS_PUBBLICITA,
        CONDENAST,
        DAZN,
        DISCOVERY_MEDIA,
        EDREAMS_ODIGEO,
        EURONEWS,
        ITALIA_ON_LINE,
        LUNIFIN_SRL,
        NET_ADDICTION,
        NEWSONLINE_IOL,
        PIEMME_ONLINE,
        RAI_PUBBLICITA,
        RAKUTEN_ADV,
        RCS_PUBBLICITA,
        SKY,
        SPORT_NETWORK,
        SUBITO,
        TRIBOO,
        WEB_SYSTEM_24,
        SPOTIFY,
        ALIBABA,
        AOL,
        AUTOSCOUT,
        INNOVATIVE_PUBLISHING,
        LASTMINUTE,
        MAIL_RU,
        MANZONI,
        MEDIAMOND,
        MOBILE_DE,
        SOGOU,
        SPEED,
        VISIBILIA,
        LIVE_QUIZ,
        GLOBO_COM,
        UOL,
        R7,
        AZIENDA_BANCA,
        BFC_MEDIA,
        DISNEY,
        ONE_FOOTBALL,
        LA_MESCOLANZA,
        MOONDO,
        IMS,
        START_MAGAZINE,
        IL_SECOLO_D_ITALIA,
        GLI_STARI_GENERALI,
        AMATE_SPONDE,
        BUZZOOLE,
        CAPACITY,
        COMMUNICATION_AFRICA,
        KADIUM,
        LA_NOTIZIA,
        THE_POST_INTERNAZIONALE,
        S4M,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        HOOPYGANG,
        LE_MONDE,
        LEQUIPE,
        LA_VANGUARDIA,
        CHILI,
        CARGURUS,
        EDMUNDS,
        KBB,
        MANEKI,
        DEEZER,
        ADPLAY,
        SEGUGIO_IT,
        VOZ_DAS_COMUNIDADES,
    ),
    NATIVE: (
        AL_FEMMINILE,
        CIAOPEOPLE_ADV_FANPAGE,
        EURONEWS,
        SPORT_NETWORK,
        SUBITO,
        CITYNEWS,
        MELHOR_ESCOLHA,
        MAGGIOLI,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        ITALIA_ON_LINE,
        MANZONI,
        PIEMME_ONLINE,
        MANEKI,
        CRM_SRL_AUTOMOTO_IT,
        DAICAR,
        RCS,
        CAR_AFFINITY,
        EUROMEDIA,
        INDUSTRIA_ITALIANA,
        MOTORIONLINE,
        SPEED,
        CAVAL,
        AUTOXY,
        DRIVEK,
        BAKECA,
        AUTOSCOUT,
        MEDIAMOND,
        MOBILE_DE,
        NEWSONLINE_IOL,
        GMAIL,
        KBB,
        EBAY,
        GLI_STARI_GENERALI,
        DEEZER,
        ADPLAY,
        RCS_PUBBLICITA,
        COMPARA_PLANO,
    ),
    MOBILE: (
        CITYNEWS,
        SOS_TARIFFE,
        FACILE_IT,
        LIVE_QUIZ,
        SHOPFULLY,
        BFC_MEDIA,
        DISNEY,
        MOONDO,
        IMS,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        CHILI,
        TRIBOO,
        AUTOSCOUT,
        CIAOPEOPLE_ADV_FANPAGE,
        MANZONI,
        MEDIAMOND,
        NET_ADDICTION,
        NEWSONLINE_IOL,
        PIEMME_ONLINE,
        SKY,
        SUBITO,
        WEB_SYSTEM_24,
        AOL,
        RAI_PUBBLICITA,
        RCS_PUBBLICITA,
        CARGURUS,
        EDMUNDS,
        KBB,
        UOL,
        EBAY,
        VALEMEDIA_SRL,
        ART_TRIBUNE,
        MONEY_DISPLAY,
        DAVIDE_MAGGIO,
        BANCA_FORTE,
        TRAVEL_ALLIANCE,
        MANEKI,
        DEEZER,
        WAZE,
        SEGUGIO_IT,
    ),
    DEM: (
        TRIBOO,
        MANZONI,
        ITALIA_ON_LINE,
        IGOAL,
        MAGGIOLI,
        SPORT_NETWORK,
        MEDIAMOND,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        RCS,
        WEBBDONE,
        INDUSTRIA_ITALIANA,
        LASTMINUTE,
        WEB_SYSTEM_24,
        ADSALSA,
        ADVERTISME,
        VERTIGO,
        ADPLAY,
        RCS_PUBBLICITA,
        DIGITOO,
        IMMOBILIARE,
    ),
    PROGRAMMATIC_RADIO: (
        SPOTIFY,
        TRIBOO,
        S4M,
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        MEDIAMOND,
        RTL,
        RDS,
        TENCENT_MUSIC_SPOTIFY,
        DEEZER,
        PANDORA,
    ),
    DOOH: (
        S4M,
        MEDIAMOND,
        RAI_PUBBLICITA,
    ),
    ADDRESSABLE_TV: (
        CARWOW,
        MEDIA_SOLUTIONS,
        VIBRANT_MEDIA,
        MEDIA_IMPACT,
        SEVEN_ONE_MEDIA,
        CHILI,
        DISCOVERY_MEDIA,
        SKY,
        SPEED,
    ),
    BRANDED_CONTENT: (
        ADPLAY,
        AL_FEMMINILE,
        ART_TRIBUNE,
        CAPACITY,
        CIAOPEOPLE_ADV_FANPAGE,
        COMMUNICATION_AFRICA,
        DAVIDE_MAGGIO,
        DIGITAL360,
        EURONEWS,
        GLI_STARI_GENERALI,
        IL_SECOLO_D_ITALIA,
        IMS,
        INNOVATIVE_PUBLISHING,
        ITALIA_ON_LINE,
        KADIUM,
        MANZONI,
        MEDIAMOND,
        MONEY_DISPLAY,
        NEWSONLINE_IOL,
        START_MAGAZINE,
        TRIBOO,
        WEB_SYSTEM_24,
    ),
    INFLUENCER_MARKETING: (
        AL_FEMMINILE,
        BUZZOOLE,
        TRIBOO,
    )
}

MEDIA_INTEGRATIONS_WITHOUT_INVALID = {
    media: label for media, label in MEDIA_INTEGRATIONS.items() if media not in INVALID_MEDIA_AD_NETWORKS
}

CHANNELS_TO_MEDIA_WITHOUT_INVALID = {
    channel: [media for media in medias if media not in INVALID_MEDIA_AD_NETWORKS]
    for channel, medias in CHANNELS_TO_MEDIA_MAP.items()
}
