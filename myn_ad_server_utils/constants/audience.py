from myn_ad_server_utils.constants.channels import *

# Special channel for audiences
DMP = 'dmp'

ADOBE = 'adobe'
LOTAME = 'lotame'
ORACLE = 'oracle'
SALESFORCE_DMP = 'salesforce_dmp'

AUDIENCE_INTEGRATIONS = {
    ADOBE: 'Adobe',
    LOTAME: 'Lotame',
    ORACLE: 'Oracle',
    SALESFORCE_DMP: 'Salesforce DMP',
}

CHANNELS_TO_AUDIENCE_INTEGRATIONS_MAP = {
    DMP: (
        ADOBE,
        LOTAME,
        ORACLE,
        SALESFORCE_DMP,
    ),
    SOCIAL: (
    )
}

AUDIENCE_CHANNELS_NAME_MAP = {
    DMP: 'DMP',
}
