#
# integrated channels
#
DISPLAY = 'display'
VIDEO = 'video'
SOCIAL = 'social'
SEARCH = 'search'

INTEGRATED_CHANNELS = {
    SEARCH: 'Search',
    SOCIAL: 'Social',
    DISPLAY: 'Display',
    VIDEO: 'Video',
}

#
# non-integrated channels
#
NATIVE = 'native'
ADDRESSABLE_TV = 'addressable_tv'
DOOH = 'dooh'
PROGRAMMATIC_RADIO = 'programmatic_radio'
DEM = 'dem'
MOBILE = 'mobile'
CHATBOT = 'chatbot'
OFFLINE_TV = 'offline_tv'
INFLUENCER_MARKETING = 'influencer_marketing'
BRANDED_CONTENT = 'branded_content'

NOT_INTEGRATED_CHANNELS = {
    NATIVE: 'Native',
    MOBILE: 'Mobile',
    DEM: 'DEM',
    PROGRAMMATIC_RADIO: 'Programmatic Radio',
    DOOH: 'DOOH',
    ADDRESSABLE_TV: 'Addressable TV',
    OFFLINE_TV: 'Offline TV',
    CHATBOT: 'Chatbot',
    INFLUENCER_MARKETING: 'Influencer Marketing',
    BRANDED_CONTENT: 'Branded Content',
}
