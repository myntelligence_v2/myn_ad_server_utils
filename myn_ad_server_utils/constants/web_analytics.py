WEB_ANALYTICS = 'web_analytics'
WEB_ANALYTICS_LABEL = 'Web Analytics'

ADOBE_ANALYTICS = 'adobe_analytics'
GOOGLE_ANALYTICS = 'google_analytics'


WEB_ANALYTICS_INTEGRATIONS = {
    ADOBE_ANALYTICS: 'Adobe Analytics',
    GOOGLE_ANALYTICS: 'Google Analytics',
}
