import enum

FREEWHEEL = 'freewheel'
CAMPAIGN_MANAGER = 'campaign_manager'
ADFORM = 'adform'
SIZMEK_AD_SERVER = 'sizmek_ad_server'
APPSFLYER_AD_SERVER = 'appsflyer_ad_server'

AD_SERVERS = {
    ADFORM: 'AdForm',
    FREEWHEEL: 'Freewheel',
    CAMPAIGN_MANAGER: 'Google Campaign Manager',
    SIZMEK_AD_SERVER: 'Sizmek',
    APPSFLYER_AD_SERVER: 'Appsflyer',
}


# Enum for ad servers that can be created on platform
class AdServerEnum(enum.Enum):
    EMPTY_AD_SERVER = ''
    ADFORM = ADFORM
    CAMPAIGN_MANAGER = CAMPAIGN_MANAGER
    SIZMEK_AD_SERVER = SIZMEK_AD_SERVER
    APPSFLYER_AD_SERVER = APPSFLYER_AD_SERVER
