from collections import namedtuple, defaultdict
from typing import List, Optional

try:
    from collections.abc import Iterable
except ImportError:
    from collections import Iterable

from myn_ad_server_utils.constants import (
    INTEGRATED_CHANNELS,
    INTEGRATED_PLATFORMS,
    CHANNELS_TO_INTEGRATED_PLATFORMS_MAP,
    NOT_INTEGRATED_CHANNELS,
    NOT_INTEGRATED_PLATFORMS,
    CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_MAP,
    INTEGRATED_TYPE,
    FUTURE_INTEGRATION_TYPE
)


PlatformDataModel = namedtuple('PlatformDataModel', 'id, name, mapping_name, type')
ChannelDataModel = namedtuple('ChannelDataModel', 'id, name, mapping_name, type')


def get_platforms_models_by_channel(channel: str) -> list:
    """Helper returning list of default platform options for given channel
    casted to Platform datamodel"""
    platforms_map = {**INTEGRATED_PLATFORMS, **NOT_INTEGRATED_PLATFORMS}
    type_map = {
        INTEGRATED_TYPE: CHANNELS_TO_INTEGRATED_PLATFORMS_MAP,
        FUTURE_INTEGRATION_TYPE: CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_MAP
    }
    channel_key = make_mapping_name(channel)
    # TODO: right now we don't have defined list for future integration mapping
    if channel_key in INTEGRATED_CHANNELS:
        platforms = [
            PlatformDataModel(id=None,
                              name=platforms_map[platform_key],
                              mapping_name=platform_key,
                              type=platform_type)
            for platform_type, channel_platform_map in type_map.items()
            for platform_key in channel_platform_map[channel_key]
        ]

        return platforms
    return []


def get_channels_models_options() -> list:
    """Helper returning list of default channel options
    casted to Channel datamodel"""
    type_map = {
        INTEGRATED_TYPE: INTEGRATED_CHANNELS,
        FUTURE_INTEGRATION_TYPE: NOT_INTEGRATED_CHANNELS
    }
    channels = [
        ChannelDataModel(id=None,
                         name=value,
                         mapping_name=key,
                         type=channel_type)
        for channel_type, channel_map in type_map.items()
        for key, value in channel_map.items()
    ]
    return channels


def make_mapping_name(name: str) -> str:
    return name.strip().replace(' ', '_').lower()


def get_available_channel_platforms(channel: str) -> set:
    channel_key = make_mapping_name(channel)
    return {
        *CHANNELS_TO_INTEGRATED_PLATFORMS_MAP[channel_key],
        *CHANNELS_TO_NOT_INTEGRATED_PLATFORMS_MAP[channel_key]
    }


def mapping_to_choices(mapping: dict) -> list:

    if not isinstance(mapping, dict):
        return []

    choices = []
    for key, value in mapping.items():
        if isinstance(value, dict):
            choices.append((key, mapping_to_choices(value)))
        else:
            choices.append((key, value))
    return choices


def merge_dicts(dicts: List[dict]) -> Optional[defaultdict]:
    merged_dict = defaultdict(list)

    for d in dicts:
        for key, value in d.items():
            if isinstance(value, Iterable) and not isinstance(value, str):
                for value_item in value:
                    merged_dict[key].append(value_item)
            else:
                merged_dict[key].append(value)

    return merged_dict
